<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
	protected $fillable = [
		'title', 'description', 'tags', 'genres', 'body', 'author_id',
	];

    /**
     * Updates links story with tags and genres.
     *  
     * @param  array  $data   
     * @return void         
     */
    public function genresAndTagsUpdate( $data ) : void
    {

        $this->genresUpdate( $data['genres'] );
        $this->tagsUpdate( $data['tags']?? null );

    }

    /**
     * Updates links story with genres.
     * 
     * @param  App\models\Story  $story  
     * @param  array             $genres
     * @return void                 
     */
    private function genresUpdate( $genres ) : void
    {

        $this->genres()->detach();

        foreach ( $genres as $genreName ) {

            $genre = Genre::where( 'name', $genreName )->first();

            $this->genres()->attach( $genre );

        }
    }
    /**
     * Updates links story with tags.
     * 
     * @param  App\models\Story  $story  
     * @param  array             $tags
     * @return void               
     */
    private function tagsUpdate( $tags ) : void
    {

        $this->tags()->detach();

        if ( is_null($tags) ) return;
 
        foreach ( $tags as $tagName ) {

            $tag = Tag::firstOrCreate( ['name' => $tagName] );

            $this->tags()->attach( $tag );

        }
    }

    //AUTHOR
    public function author()
    {
        return $this->belongsTo('App\User');
    } 

	//GENRES
	public function genres()
    {
        return $this->belongsToMany('App\models\Genre');
    }

    public function hasAnyGenres($genres)
    {
        if ($this->genres()->whereIn('name', $genres)->first()) {

            return true;

        }
        return false;
    }

    public function hasGenre($genre)
    {
        if ($this->genres()->where('name', $genre)->first()) {
            
            return true;

        }
        return false;
    }

    //TAGS
	public function tags()
    {
        return $this->belongsToMany('App\models\Tag');
    }

    public function hasAnyTags($tags)
    {
        if ($this->tags()->whereIn('name', $tags)->first()) {

            return true;

        }
        return false;
    }

    public function hasTag($tag)
    {
        if ($this->tags()->where('name', $tag)->first()) {
            
            return true;

        }
        return false;
    }
}
 