<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model {
	
    protected $fillable = [
        'name', 
    ];

    public function storys() {
		return $this->belongsToMany('App\models\Story');
	}
}
