<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

    protected $fillable = [
        'name', 
    ];

	public function storys() {
		return $this->belongsToMany('App\models\Story');
	}
}
