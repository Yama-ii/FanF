<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\avatarUpdateRequest;
use Image;

class UserController extends Controller
{
    
    public function userPage( $username ) {

        $user = User::where('name', $username)->first();
        return view('auth.user.user_page')->with('user', $user);

    }

    public function written( $username ) {

        $user = User::where('name', $username)->first();
        return view('auth.user.user_written_storis_list')->with('user', $user);

    }

    public function updateAvatar( avatarUpdateRequest $request, $username ) {

        $user = User::where('name', $username)->first();
        $user->can('update-profile', $user);

        if ( $user->avatar_name != 'User_def_avatar.png' ) { 

            $user->clearAvatar();

        }

        $avatar = $request->file('avatar');

        $fileName = time() . '_' . $user->name . '.' . $avatar->getClientOriginalExtension();

        Image::make($avatar)->resize(300, 300)->save( public_path( $user->getAvatarPath() ) . $fileName);

        $user->avatar_name = $fileName;
        $user->save();

        return back();
    }
}
