<?php

namespace App\Http\Controllers;

use App\models\Story;
use App\models\Genre;
use App\models\Tag;
use App\User;
use App\Http\Requests\StoryRequest;

class StoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $storys = Story::all();
        return view('storys.index')->with('storys', $storys);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genres = Genre::all();
        return view('storys.create')->with('genres', $genres);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoryRequest $request)
    {

        $data = $request->validated();

        $story = Story::create([

            'title' => $request->title,
            'author_id' => intval(auth()->user()->id),
            'body' => $request->body,
            'description' => $request->description,

        ]);

        $story->genresAndTagsUpdate( $data );

        return redirect()->route( 'story.show', $story->id );   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
            $story = Story::find($id);

            if ( is_null($story) ) return back()->withErrors(['msg' => 'Запись не найдена']);

            return view('storys.show')->with('story', $story);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $story = Story::find($id);
        $genreCheckedList = $story->genres->pluck('name')->toArray();

        return view('storys.edit')
             ->with('story', $story)
             ->with('genres', Genre::all())
             ->with('genreCheckedList', $genreCheckedList)
             ->with('storyTags', $story->tags->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoryRequest $request, $id)
    {
        $data = $request->validated();
        $story = Story::find($id);

        if ( !$story ) return back()->withErrors(['msg' => 'Запись не найдена'])->withInput();

        $story->update([

            'title' => $data['title'],
            'body' => $data['body'],
            'description' => $data['description'],

        ]);

        $story->genresAndTagsUpdate( $data );

        return back()->withInput()->with(['success' => 'Запись обновлена']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $story = Story::find($id);
        $story->tags()->detach();
        $story->genres()->detach();
        $story->delete();

        return redirect()->route('userPage',$story->author->name)->with(['success' => 'Запись удолена']);
    }
}
