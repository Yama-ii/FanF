<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class StoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        $rules = [
            'title' => 'required|max:255',
            'body' => 'required|min:10',
            'description' => 'required|min:10',
            'genres.*' => '',
            'genres.0' => 'required',
            'tags.*' => 'min:2|max:30',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'title.required' => 'Придумай название!',
            'title.max:255' => 'Название слишком длинное!',
            'body.required' => 'Придумай что-нибудь интересное!',
            'body.min' => 'Слишком мало!',
            'description.required' => 'Описание обязательно!',
            'description.min' => 'Описание слишком короткое!',
            'genres.0.required' => 'Нужен минимум один жанр!',
            'tags.*.min' => 'Тег слишком короткий!',
            'tags.*.max' => 'Тег слишком длинный!',
        ];
    }
}
