<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'password', 'additional_info', 'avatar_path',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    /*
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    */
   public function getAvatar()
   {
      return asset(  $this->getAvatarPath(). $this->avatar_name );
   }

    public function getAvatarPath()
    {
        return 'img/avatars/';
    }

    public function clearAvatar()
    {
        $avatarPath = public_path( $this->getAvatarPath() );

        unlink( $avatarPath.$this->avatar_name );
    }

    //STIRYS
    public function storys()
    {
        return $this->hasMany('App\models\Story', 'author_id');
    }

    //ROLES
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function hasAnyRoles($roles)
    {
        if ($this->roles()->whereIn('name', $roles)->first()) {

            return true;

        }
        return false;
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            
            return true;

        }
        return false;
    }

    //GRADES
    public function grades()
    {
    return $this->belongsToMany('App\models\Grade');
    }

    public function hasAnyGrades($grades)
    {
        if ($this->grades()->whereIn('grade', $grades)->first()) {

            return true;

        }
        return false;
    }

    public function hasGrade($grade)
    {
        if ($this->grades()->where('grade', $grade)->first()) {
            
            return true;

        }
        return false;
    }
}
