const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js')
   .js('resources/js/Tags.js', 'public/js')
   .js('resources/js/colorThemeController.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .sass('resources/sass/reset.scss', 'public/css');