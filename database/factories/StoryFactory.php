<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\models\Story;
use Faker\Generator as Faker;

$factory->define(Story::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(rand(2, 4), true),
        'description' => $faker->realText(rand(200, 700)),
        'body' => $faker->realText(rand(2000, 10000)),
        'author_id' => rand(4, 50),
    ];
});
