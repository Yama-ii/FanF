<?php

use Illuminate\Database\Seeder;
use App\models\Tag;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::truncate();

        Tag::create(['name' => 'horrorTag']);
        Tag::create(['name' => 'comedyTag']);
    }
}
