<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(GenresTableSeeder::class);
         $this->call(TagTableSeeder::class);
         $this->call(StoryTableSeeder::class);
         $this->call(RolesTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $users = factory(\App\User::class, 100)->create();
         $tags = factory(\App\models\Tag::class, 10)->create();
         $storys =  factory(\App\models\Story::class, 500)->create();

         foreach ( $users as $user ) {

            $user->roles()->attach(3);

         }

         foreach ( $storys as $story ) {
            for ( $i = 1; $i <= rand(1, 10); $i++ ) {
                $story->tags()->attach(rand(1, 12));
            }
            for ( $i = 1; $i <= rand(1, 5); $i++ ) {
                $story->genres()->attach(rand(1, 5));
            }
            

         }
    }
}
