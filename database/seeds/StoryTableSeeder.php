<?php

use Illuminate\Database\Seeder;
use App\models\Story;
use App\models\Genre;
use App\models\Tag;

class StoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Story::truncate();
        DB::table('story_tag')->truncate();
        DB::table('genre_story')->truncate();
    }
}
