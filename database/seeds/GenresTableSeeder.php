<?php

use Illuminate\Database\Seeder;
use App\models\Genre;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Genre::truncate();

        Genre::create(['name' => 'horror']);
        Genre::create(['name' => 'comedy']);
        Genre::create(['name' => 'drama']);
        Genre::create(['name' => 'thriller']);
        Genre::create(['name' => 'crime']);
    }
}
