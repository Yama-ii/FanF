<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	User::truncate();
    	DB::table('role_user')->truncate();
        //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
    	$adminRole = Role::where('name', 'admin')->first();

    	$admin = User::create([
    		'name' => 'Admin',
    		//'email' => 'gg_vpvp2@mail.ru',
    		'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
    	]);

    	$admin->roles()->attach($adminRole);
    }
}
