@extends('layouts.app')

@section('content')
 	 @foreach($storys as $story)
	    <div>
	    	<a href="{{ url('/story/'.$story->id) }}">
		    	<h2>
			    	{{$story->id}}.
			   	 	{{$story->title}}
			   	 	@can('update-story', $story)
			   	 		@include('inc.story_manage')
					@endcan
		   	 	</h2>
	   	 	</a>
	   	 	<br>
	   	 	<div>
	   	 		<h3>description</h3>
	   	 		<p>
	   	 			{{$story->description}}
	   	 		</p>
	   	 	</div>
	   	 	<br>
	   	 	<div>
	   	 		<h3>Tags</h3>
	   	 		<ol class="tags_list">
	   	 		@foreach($story->tags as $tag)
	   	 			<li>{{ $tag->name }}</li>
	   	 		@endforeach
	   	 		</ol>
	   	 	</div>
	   	 	<br>
			<div>
				<h3>Genres</h3>
	   	 		<ol class="genres_list">
	   	 		@foreach($story->genres as $genre)
	   	 			<li>{{ $genre->name }}</li>
	   	 		@endforeach
	   	 		</ol>
	   	 	</div>
		</div>
		<hr>
	@endforeach
@endsection