@extends('layouts.app')

@section('headers')
<script src="{{ asset('js/Tags.js') }}" defer></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/invalid.css') }}">
@endsection

@section('content') 
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">{{ __('Добавить статью') }}</div>

            <div class="card-body">
                <form method="POST" action="{{ route('story.store') }}">
                	@csrf

                	<div class="form-group row">
                        <label for="title" class="col-md-2 col-form-label text-md-right">{{ __('Название') }}</label>

                        <div class="col-md-10">
                            <input id="title" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" autocomplete="title" autofocus>

                            @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="body" class="col-md-2 col-form-label text-md-right">{{ __('Содержание') }}</label>

                        <div class="col-md-10">
                            <textarea id="body" rows="50" cols="30" class="form-control @error('body') is-invalid @enderror" name="body" autocomplete="body" autofocus>{{ old('body') }}</textarea>

                            @error('body')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="description" class="col-md-2 col-form-label text-md-right">{{ __('Описание') }}</label>

                        <div class="col-md-10">
                            <textarea id="description" rows="5" cols="30" class="form-control @error('description') is-invalid @enderror" name="description"  autocomplete="description" autofocus>{{ old('description') }}</textarea>

                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                   @include('inc.genre_form')

                   @include('inc.tag_form')

                   <input type="hidden" name="author_id" value="{{ Auth::user()->id }}">
                    <div class="form-group row">
                        <div class="col-md-10">
                            <input type="submit" name="submit" id="submit" value="Отправить">
                        </div>
                    </div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection