@extends('layouts.app')

@section('headers')

@endsection

@section('content')
<h1>{{$story->title}}</h1>
<a href="{{ url('/story/'.$story->id) }}">
	<h2>
		@can('update-story', $story)
   	 		@include('inc.story_manage')
   		 @endcan
	 </h2>
 </a>
<p>{{$story->body}}</p>
@endsection