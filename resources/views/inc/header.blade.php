<header>
    <div class="brand">
        <a href="{{ route('home') }}">
            <img src="" alt="{{ config('app.name', 'FanF') }}">    
        </a>
    </div>
    <nav>
        <ul class="navbar">
            <!-- Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                <li class="dropdown nav-item">
                    <a id="navbarDropdown" class="dropdown-lebel" href="{{ url('/user/'.Auth::user()->name) }}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <span>{{ Auth::user()->name }}</span>
                        <div class="avathar">
                            <img src="{{ Auth::user()->getAvatar() }}" alt="av">
                        </div>
                    </a>
                    <div class="dropdown-menu">
                        <ul class="dropdown-list">
                            @can('manage-users')
                            <li class="dropdown-item">
                                <a class="" href="{{route('admin.users.index')}}">
                                    User Managment
                                </a>
                            </li>
                            @endcan
                            <li class="dropdown-item">
                                <a class="" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </li>
            @endguest
            <li class="theme nav-item">
                <label for="theme">Dark</label>
                <input type="checkbox" name="theme" id="theme-checkbox">
            </li>
            @auth
                <li class="nav-item">
                    <a class="" href="{{ url('/story/create') }}">
                        +
                    </a>
                </li>
            @endauth
        </ul>
    </nav>
</header>