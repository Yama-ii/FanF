@error('genres.0')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror
@foreach($genres as $genre)
    <div class="form-group row">
        <label for="{{ $genre->name }}" class="col-md-2 col-form-label text-md-right">{{ $genre->name }}</label>

        <div class="col-md-10">
            <input type="checkbox" id="{{ $genre->name }}" class="form-control @error('genres.0') is-invalid @enderror" name="genres[]"  autocomplete="genre" value="{{ $genre->name }}"

                <?php 
                    // Checking for an Old Value
                    for ( $i=0; $i < 25; $i++ ) {
                        if ( $genre->name == old( 'genres.'.$i ) ) {
                            echo 'checked ';
                        }
                    }
                    // Checking for an Changed Values
                    if (isset($genreCheckedList)) {
                        foreach ( $genreCheckedList as $genreChecked) {
                            if ( $genreChecked == $genre->name ) {
                                echo 'checked';
                            }
                        }
                    }
                    
                ?>
            >
        </div>
    </div>
@endforeach