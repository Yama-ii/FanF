<div class="form-group row">
    <label for="tags[]" class="col-md-2 col-form-label text-md-right" id="label_for_tags">{{ __('Теги') }}</label>
    <button id="add_tag_btn" type="button" style="display: inline;">+</button>
    <ul id="tag_list" class="row col-12">
        @php $oldForm = false; @endphp

        @for($i = 0; $i < 25; $i++)
            @if( old('tags.'.$i) )
                <button id="delete_tag{{$i}}" type="button">x</button>
                <li class="col-12" id="tag{{$i}}_li">
                    <input type="text" id="tag{{$i}}" name="tags[]" value="{{ old('tags.'.intval($i)) }}" class="form-control @error('tags.'.intval($i)) is-invalid @enderror">
                    @error('tags.'.intval($i))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    
                </li>
                @php $oldForm = true; @endphp
            @endif
        @endfor

        @if( isset($storyTags) &&  !$oldForm)
            @foreach( $storyTags as $tag )
                <button id="delete_tag{{$loop->index}}" type="button">x</button>
                <li class="col-12" id="tag{{$loop->index}}_li">
                    <input type="text" id="tag{{$loop->index}}" name="tags[]" value="{{ $tag->name }}" class="form-control @error('tags.'.$loop->index) is-invalid @enderror">
                    @error('tags.'.$loop->index)
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    
                </li>
            @endforeach
        @endif
    </ul>
</div>