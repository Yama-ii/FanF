<a href="{{ url('/story/'. $story->id .'/edit') }}">Редактировать</a>
<form method="post" action="{{ route('story.destroy', $story->id) }}">
    @method('DELETE')
    @csrf
    <div class="form-item">
        <button type="submit" class="btn-danger">Удолить</button>
    </div>
</form>