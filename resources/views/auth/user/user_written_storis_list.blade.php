@extends('layouts.app')

@section('content')
<div>
    <div>
        <div>
            <div>
                <h1>{{ $user->name }}</h1>
                <hr>
                <ol>
                @foreach( $user->storys as $story )
                	<li>
                		<a href="{{ route('story.show', $story->id) }}"><h2>{{ $story->title }}</h2></a>
                        @can('update-story', $story)
                            @include('inc.story_manage')
                        @endcan
                		<p>{{ $story->description }}</p>
                	</li>
                	<hr>
                @endforeach
            	</ol>
            </div>
        </div>
    </div>
</div>
@endsection