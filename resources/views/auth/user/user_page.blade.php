@extends('layouts.app')

@section('content')
<div class="user_card">
	<div class="user_avatar">
		<img src="{{ $user->getAvatar() }}" alt="av">
		@can('update-profile', $user)
		<form action="{{route('updateAvatar', $user->name)}}" enctype="multipart/form-data" method="POST" id="avatar-form">
			@csrf
			<label class="add_avatar">
				<input type="file" name="avatar" id="uploade-avatar" 
					onchange="document.getElementById('avatar-form').submit();">
				<span>Загрузить аватар</span>
			</label>
		</form>
		@endcan 
	</div>
	<div class="user_name">
		<h1>{{ $user->name }}</h1>
	</div>
	<a href="{{ url('/user/'.$user->name.'/written') }}">written({{ count($user->storys) }})</a>
</div>
@endsection
