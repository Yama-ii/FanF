@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Users</div>
                @foreach($users as $user)
                    <div><a href="{{ url('/user/'.$user->name) }}">{{$user->name}}</a> - {{implode(', ', $user->roles()->get()->pluck('name')->toArray())}}</div>

                    @can('edit-users')

                    <a href="{{route('admin.users.edit', $user->id)}}"><button>Edit</button></a>

                    @endcan

                    @can('delete-users')
                    
                    <form action="{{Route('admin.users.destroy', $user)}}" method="POST">
                    @csrf
                    {{method_field('DELETE')}}
                        <button type="submit">
                            Delite
                        </button>
                    </form>

                    @endcan
                    <hr>
                @endforeach
            <div class="card-body">
               
            </div>
        </div>
    </div>
</div>
@endsection
