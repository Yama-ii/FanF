let add_tag_btn = document.getElementById( 'add_tag_btn' );
let tag_ul = document.getElementById( 'tag_list' );
let label_for_tags = document.getElementById( 'label_for_tags' );

let tag_max_number = 10;
let tag_num = 0;
let add_tag_num = 0;

let add_tag_possible  = false;

for ( let i = 0; i <= 20; i++ ) {

	let tag_input =  document.getElementById( 'tag' + i );

	if ( tag_input != null ) {

		let tag_li =  document.getElementById( 'tag' + i + '_li' );
		let tag_delete_btn =  document.getElementById( 'delete_tag' + i );

		tag_delete_btn.addEventListener( 'click', function() {
			tag_li.remove();
			tag_delete_btn.remove();
			--tag_num;

			if ( document.getElementById( 'add_tag_btn' ) == null ) {

				label_for_tags.after( add_tag_btn );

			}
		});

		tag_num += 1;

	}
}

if ( document.getElementById( 'add_tag_btn' ) != null ) {

	add_tag_btn.addEventListener( 'click', addTag );

	if ( tag_num == 10 ) {

		add_tag_btn.remove();

	}
};

function addTag() {

	tag_num += 1;

	for ( let i = 0; i <= tag_max_number; i++ ) {
		
		let tag_input = document.getElementById( 'tag' + i );

		if ( tag_input == null ) {

			add_tag_num = i;
			add_tag_possible = true;

			break;

		}
	}

	if ( add_tag_possible ) {

		let tag_li = document.createElement( 'li' );
		tag_li.classList.add( 'col-12' );
		tag_li.id = 'tag' + add_tag_num + '_li';

		let tag_input = document.createElement( 'input' );
		tag_input.classList.add( 'form-control' );
		tag_input.setAttribute( "id", 'tag' + add_tag_num );
		tag_input.setAttribute( "name", 'tags[]' );

		let tag_delete_btn = document.createElement( 'button' );
		tag_delete_btn.id = 'delete_' + add_tag_num;
		tag_delete_btn.textContent = 'x';
		tag_delete_btn.type = 'button';

		tag_delete_btn.onclick = function () {

			tag_li.remove();
			tag_delete_btn.remove();
			--tag_num;

			if (document.getElementById( 'add_tag_btn' ) == null ) {

				label_for_tags.after( add_tag_btn );

			}
		};

		tag_ul.prepend( tag_li );
		tag_li.prepend( tag_input );
		tag_li.before( tag_delete_btn );
	}

	if  ( tag_num == tag_max_number || !add_tag_possible ) {

		add_tag_btn.remove();

	}

	add_tag_possible = false;
}