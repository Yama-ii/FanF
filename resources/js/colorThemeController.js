let themeCheckbox = document.getElementById( 'theme-checkbox' );

themeCheckbox.onchange = function() {

	if (this.checked) {
		document.body.setAttribute('dark', '');
		localStorage.setItem( 'theme', 'dark' );
	} else {
		document.body.removeAttribute('dark');
		localStorage.removeItem( 'theme' );

	}
}

if ( localStorage.getItem('theme') == 'dark' ) {
	themeCheckbox.checked = true;
	document.body.setAttribute('dark', '');
}
