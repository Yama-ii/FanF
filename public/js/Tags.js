/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/Tags.js":
/*!******************************!*\
  !*** ./resources/js/Tags.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

var add_tag_btn = document.getElementById('add_tag_btn');
var tag_ul = document.getElementById('tag_list');
var label_for_tags = document.getElementById('label_for_tags');
var tag_max_number = 10;
var tag_num = 0;
var add_tag_num = 0;
var add_tag_possible = false;

for (var i = 0; i <= 20; i++) {
  var tag_input = document.getElementById('tag' + i);

  if (tag_input != null) {
    (function () {
      var tag_li = document.getElementById('tag' + i + '_li');
      var tag_delete_btn = document.getElementById('delete_tag' + i);
      tag_delete_btn.addEventListener('click', function () {
        tag_li.remove();
        tag_delete_btn.remove();
        --tag_num;

        if (document.getElementById('add_tag_btn') == null) {
          label_for_tags.after(add_tag_btn);
        }
      });
      tag_num += 1;
    })();
  }
}

if (document.getElementById('add_tag_btn') != null) {
  add_tag_btn.addEventListener('click', addTag);

  if (tag_num == 10) {
    add_tag_btn.remove();
  }
}

;

function addTag() {
  tag_num += 1;

  for (var _i = 0; _i <= tag_max_number; _i++) {
    var _tag_input = document.getElementById('tag' + _i);

    if (_tag_input == null) {
      add_tag_num = _i;
      add_tag_possible = true;
      break;
    }
  }

  if (add_tag_possible) {
    var tag_li = document.createElement('li');
    tag_li.classList.add('col-12');
    tag_li.id = 'tag' + add_tag_num + '_li';

    var _tag_input2 = document.createElement('input');

    _tag_input2.classList.add('form-control');

    _tag_input2.setAttribute("id", 'tag' + add_tag_num);

    _tag_input2.setAttribute("name", 'tags[]');

    var tag_delete_btn = document.createElement('button');
    tag_delete_btn.id = 'delete_' + add_tag_num;
    tag_delete_btn.textContent = 'x';
    tag_delete_btn.type = 'button';

    tag_delete_btn.onclick = function () {
      tag_li.remove();
      tag_delete_btn.remove();
      --tag_num;

      if (document.getElementById('add_tag_btn') == null) {
        label_for_tags.after(add_tag_btn);
      }
    };

    tag_ul.prepend(tag_li);
    tag_li.prepend(_tag_input2);
    tag_li.before(tag_delete_btn);
  }

  if (tag_num == tag_max_number || !add_tag_possible) {
    add_tag_btn.remove();
  }

  add_tag_possible = false;
}

/***/ }),

/***/ 1:
/*!************************************!*\
  !*** multi ./resources/js/Tags.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\ospanel\domains\FanF\resources\js\Tags.js */"./resources/js/Tags.js");


/***/ })

/******/ });