<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/user/{name}', 'UserController@userPage')->name('userPage');
Route::get('/user/{name}/written', 'UserController@written');
Route::post('/user/{name}/avatar_update', 'UserController@updateAvatar')->name('updateAvatar');

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function (){
	Route::resource('/users', 'UsersController', ['except' => ['show', 'create', 'store']]);
});

Route::resource('/story', 'StoryController');
Route::get('/story/grading', 'GradeController@grading')->name('grading');